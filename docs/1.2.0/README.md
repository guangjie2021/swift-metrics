# SwiftMetrics Docs

SwiftMetrics is a Swift metrics API package.

To get started with SwiftMetrics, [`import Metrics`](../CoreMetrics/index.html). The most important types are:

* [`Counter`](https://apple.github.io/swift-metrics/docs/current/CoreMetrics/Classes/Counter.html)
* [`Timer`](https://apple.github.io/swift-metrics/docs/current/CoreMetrics/Classes/Timer.html)
* [`Recorder`](https://apple.github.io/swift-metrics/docs/current/CoreMetrics/Classes/Recorder.html)
* [`Gauge`](https://apple.github.io/swift-metrics/docs/current/CoreMetrics/Classes/Gauge.html)

SwiftMetrics contains multiple modules:
 - [CoreMetrics](../CoreMetrics/index.html)
 - [Metrics](../Metrics/index.html)
