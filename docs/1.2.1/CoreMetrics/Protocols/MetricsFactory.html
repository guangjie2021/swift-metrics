<!DOCTYPE html>
<html lang="en">
  <head>
    <title>MetricsFactory Protocol Reference</title>
    <link rel="stylesheet" type="text/css" href="../css/jazzy.css" />
    <link rel="stylesheet" type="text/css" href="../css/highlight.css" />
    <meta charset="utf-8">
    <script src="../js/jquery.min.js" defer></script>
    <script src="../js/jazzy.js" defer></script>
    
    <script src="../js/lunr.min.js" defer></script>
    <script src="../js/typeahead.jquery.js" defer></script>
    <script src="../js/jazzy.search.js" defer></script>
  </head>
  <body>

    <a name="//apple_ref/swift/Protocol/MetricsFactory" class="dashAnchor"></a>

    <a title="MetricsFactory Protocol Reference"></a>

    <header class="header">
      <p class="header-col header-col--primary">
        <a class="header-link" href="../index.html">
          CoreMetrics 1.2.1 Docs
        </a>
         (81% documented)
      </p>
    
      <p class="header-col--secondary">
        <form role="search" action="../search.json">
          <input type="text" placeholder="Search documentation" data-typeahead>
        </form>
      </p>
    
        <p class="header-col header-col--secondary">
          <a class="header-link" href="https://github.com/apple/swift-metrics">
            <img class="header-icon" src="../img/gh.png"/>
            View on GitHub
          </a>
        </p>
    
        <p class="header-col header-col--secondary">
          <a class="header-link" href="dash-feed://https%3A%2F%2Fapple%2Egithub%2Eio%2Fswift%2Dmetrics%2Fdocs%2F1%2E2%2E1%2FCoreMetrics%2Fdocsets%2FCoreMetrics%2Exml">
            <img class="header-icon" src="../img/dash.png"/>
            Install in Dash
          </a>
        </p>
    </header>

    <p class="breadcrumbs">
      <a class="breadcrumb" href="../index.html">CoreMetrics Reference</a>
      <img class="carat" src="../img/carat.png" />
      MetricsFactory Protocol Reference
    </p>

    <div class="content-wrapper">
      <nav class="navigation">
        <ul class="nav-groups">
          <li class="nav-group-name">
            <a class="nav-group-name-link" href="../Classes.html">Classes</a>
            <ul class="nav-group-tasks">
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Classes/Counter.html">Counter</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Classes/Gauge.html">Gauge</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Classes/MultiplexMetricsHandler.html">MultiplexMetricsHandler</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Classes/NOOPMetricsHandler.html">NOOPMetricsHandler</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Classes/Recorder.html">Recorder</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Classes/Timer.html">Timer</a>
              </li>
            </ul>
          </li>
          <li class="nav-group-name">
            <a class="nav-group-name-link" href="../Enums.html">Enumerations</a>
            <ul class="nav-group-tasks">
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Enums/MetricsSystem.html">MetricsSystem</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Enums/TimeUnit.html">TimeUnit</a>
              </li>
            </ul>
          </li>
          <li class="nav-group-name">
            <a class="nav-group-name-link" href="../Protocols.html">Protocols</a>
            <ul class="nav-group-tasks">
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Protocols/CounterHandler.html">CounterHandler</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Protocols/MetricsFactory.html">MetricsFactory</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Protocols/RecorderHandler.html">RecorderHandler</a>
              </li>
              <li class="nav-group-task">
                <a class="nav-group-task-link" href="../Protocols/TimerHandler.html">TimerHandler</a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <article class="main-content">

        <section class="section">
          <div class="section-content top-matter">
            <h1>MetricsFactory</h1>
              <div class="declaration">
                <div class="language">
                  <pre class="highlight swift"><code><span class="kd">public</span> <span class="kd">protocol</span> <span class="kt">MetricsFactory</span></code></pre>

                </div>
              </div>
            <p>The <code>MetricsFactory</code> is the bridge between the <code><a href="../Enums/MetricsSystem.html">MetricsSystem</a></code> and the metrics backend implementation.
<code>MetricsFactory</code>&lsquo;s role is to initialize concrete implementations of the various metric types:</p>

<ul>
<li><code><a href="../Classes/Counter.html">Counter</a></code> -&gt; <code><a href="../Protocols/CounterHandler.html">CounterHandler</a></code></li>
<li><code><a href="../Classes/Recorder.html">Recorder</a></code> -&gt; <code><a href="../Protocols/RecorderHandler.html">RecorderHandler</a></code></li>
<li><p><code><a href="../Classes/Timer.html">Timer</a></code> -&gt; <code><a href="../Protocols/TimerHandler.html">TimerHandler</a></code></p></li>
</ul><div class="aside aside-warning">
    <p class="aside-title">Warning</p>
    <p>This type is an implementation detail and should not be used directly, unless implementing your own metrics backend.
       To use the SwiftMetrics API, please refer to the documentation of <code><a href="../Enums/MetricsSystem.html">MetricsSystem</a></code>.</p>

</div>
<h1 id='destroying-metrics' class='heading'>Destroying metrics</h1>

<p>Since <em>some</em> metrics implementations may need to allocate (potentially &ldquo;heavy&rdquo;) resources for metrics, destroying
metrics offers a signal to libraries when a metric is &ldquo;known to never be updated again.&rdquo;</p>

<p>While many metrics are bound to the entire lifetime of an application and thus never need to be destroyed eagerly,
some metrics have well defined unique life-cycles, and it may be beneficial to release any resources held by them
more eagerly than awaiting the application&rsquo;s termination. In such cases, a library or application should invoke
a metric&rsquo;s appropriate <code>destroy()</code> method, which in turn results in the corresponding handler that it is backed by
to be passed to <code>destroyCounter(handler:)</code>, <code>destroyRecorder(handler:)</code> or <code>destroyTimer(handler:)</code> where the factory
can decide to free any corresponding resources.</p>

<p>While some libraries may not need to implement this destroying as they may be stateless or similar,
libraries using the metrics API should always assume a library WILL make use of this signal, and shall not
neglect calling these methods when appropriate.</p>

          </div>
        </section>

        <section class="section">
          <div class="section-content">
            <div class="task-group">
              <ul class="item-container">
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:11CoreMetrics0B7FactoryP11makeCounter5label10dimensionsAA0E7Handler_pSS_SaySS_SStGtF"></a>
                    <a name="//apple_ref/swift/Method/makeCounter(label:dimensions:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:11CoreMetrics0B7FactoryP11makeCounter5label10dimensionsAA0E7Handler_pSS_SaySS_SStGtF">makeCounter(label:dimensions:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Create a backing <code><a href="../Protocols/CounterHandler.html">CounterHandler</a></code>.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">func</span> <span class="nf">makeCounter</span><span class="p">(</span><span class="nv">label</span><span class="p">:</span> <span class="kt">String</span><span class="p">,</span> <span class="nv">dimensions</span><span class="p">:</span> <span class="p">[(</span><span class="kt">String</span><span class="p">,</span> <span class="kt">String</span><span class="p">)])</span> <span class="o">-&gt;</span> <span class="kt"><a href="../Protocols/CounterHandler.html">CounterHandler</a></span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>label</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The label for the <code><a href="../Protocols/CounterHandler.html">CounterHandler</a></code>.</p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <code>
                                <em>dimensions</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The dimensions for the <code><a href="../Protocols/CounterHandler.html">CounterHandler</a></code>.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="slightly-smaller">
                        <a href="https://github.com/apple/swift-metrics/tree/1.2.1/Sources/CoreMetrics/Metrics.swift#L400">Show on GitHub</a>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:11CoreMetrics0B7FactoryP12makeRecorder5label10dimensions9aggregateAA0E7Handler_pSS_SaySS_SStGSbtF"></a>
                    <a name="//apple_ref/swift/Method/makeRecorder(label:dimensions:aggregate:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:11CoreMetrics0B7FactoryP12makeRecorder5label10dimensions9aggregateAA0E7Handler_pSS_SaySS_SStGSbtF">makeRecorder(label:dimensions:aggregate:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Create a backing <code><a href="../Protocols/RecorderHandler.html">RecorderHandler</a></code>.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">func</span> <span class="nf">makeRecorder</span><span class="p">(</span><span class="nv">label</span><span class="p">:</span> <span class="kt">String</span><span class="p">,</span> <span class="nv">dimensions</span><span class="p">:</span> <span class="p">[(</span><span class="kt">String</span><span class="p">,</span> <span class="kt">String</span><span class="p">)],</span> <span class="nv">aggregate</span><span class="p">:</span> <span class="kt">Bool</span><span class="p">)</span> <span class="o">-&gt;</span> <span class="kt"><a href="../Protocols/RecorderHandler.html">RecorderHandler</a></span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>label</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The label for the <code><a href="../Protocols/RecorderHandler.html">RecorderHandler</a></code>.</p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <code>
                                <em>dimensions</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The dimensions for the <code><a href="../Protocols/RecorderHandler.html">RecorderHandler</a></code>.</p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <code>
                                <em>aggregate</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>Is data aggregation expected.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="slightly-smaller">
                        <a href="https://github.com/apple/swift-metrics/tree/1.2.1/Sources/CoreMetrics/Metrics.swift#L408">Show on GitHub</a>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:11CoreMetrics0B7FactoryP9makeTimer5label10dimensionsAA0E7Handler_pSS_SaySS_SStGtF"></a>
                    <a name="//apple_ref/swift/Method/makeTimer(label:dimensions:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:11CoreMetrics0B7FactoryP9makeTimer5label10dimensionsAA0E7Handler_pSS_SaySS_SStGtF">makeTimer(label:dimensions:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Create a backing <code><a href="../Protocols/TimerHandler.html">TimerHandler</a></code>.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">func</span> <span class="nf">makeTimer</span><span class="p">(</span><span class="nv">label</span><span class="p">:</span> <span class="kt">String</span><span class="p">,</span> <span class="nv">dimensions</span><span class="p">:</span> <span class="p">[(</span><span class="kt">String</span><span class="p">,</span> <span class="kt">String</span><span class="p">)])</span> <span class="o">-&gt;</span> <span class="kt"><a href="../Protocols/TimerHandler.html">TimerHandler</a></span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>label</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The label for the <code><a href="../Protocols/TimerHandler.html">TimerHandler</a></code>.</p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <code>
                                <em>dimensions</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The dimensions for the <code><a href="../Protocols/TimerHandler.html">TimerHandler</a></code>.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="slightly-smaller">
                        <a href="https://github.com/apple/swift-metrics/tree/1.2.1/Sources/CoreMetrics/Metrics.swift#L415">Show on GitHub</a>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:11CoreMetrics0B7FactoryP14destroyCounteryyAA0E7Handler_pF"></a>
                    <a name="//apple_ref/swift/Method/destroyCounter(_:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:11CoreMetrics0B7FactoryP14destroyCounteryyAA0E7Handler_pF">destroyCounter(_:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Invoked when the corresponding <code><a href="../Classes/Counter.html">Counter</a></code>&lsquo;s <code>destroy()</code> function is invoked.
Upon receiving this signal the factory may eagerly release any resources related to this counter.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">func</span> <span class="nf">destroyCounter</span><span class="p">(</span><span class="n">_</span> <span class="nv">handler</span><span class="p">:</span> <span class="kt"><a href="../Protocols/CounterHandler.html">CounterHandler</a></span><span class="p">)</span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>handler</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The handler to be destroyed.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="slightly-smaller">
                        <a href="https://github.com/apple/swift-metrics/tree/1.2.1/Sources/CoreMetrics/Metrics.swift#L422">Show on GitHub</a>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:11CoreMetrics0B7FactoryP15destroyRecorderyyAA0E7Handler_pF"></a>
                    <a name="//apple_ref/swift/Method/destroyRecorder(_:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:11CoreMetrics0B7FactoryP15destroyRecorderyyAA0E7Handler_pF">destroyRecorder(_:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Invoked when the corresponding <code><a href="../Classes/Recorder.html">Recorder</a></code>&lsquo;s <code>destroy()</code> function is invoked.
Upon receiving this signal the factory may eagerly release any resources related to this recorder.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">func</span> <span class="nf">destroyRecorder</span><span class="p">(</span><span class="n">_</span> <span class="nv">handler</span><span class="p">:</span> <span class="kt"><a href="../Protocols/RecorderHandler.html">RecorderHandler</a></span><span class="p">)</span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>handler</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The handler to be destroyed.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="slightly-smaller">
                        <a href="https://github.com/apple/swift-metrics/tree/1.2.1/Sources/CoreMetrics/Metrics.swift#L429">Show on GitHub</a>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:11CoreMetrics0B7FactoryP12destroyTimeryyAA0E7Handler_pF"></a>
                    <a name="//apple_ref/swift/Method/destroyTimer(_:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:11CoreMetrics0B7FactoryP12destroyTimeryyAA0E7Handler_pF">destroyTimer(_:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Invoked when the corresponding <code><a href="../Classes/Timer.html">Timer</a></code>&lsquo;s <code>destroy()</code> function is invoked.
Upon receiving this signal the factory may eagerly release any resources related to this timer.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">func</span> <span class="nf">destroyTimer</span><span class="p">(</span><span class="n">_</span> <span class="nv">handler</span><span class="p">:</span> <span class="kt"><a href="../Protocols/TimerHandler.html">TimerHandler</a></span><span class="p">)</span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>handler</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The handler to be destroyed.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="slightly-smaller">
                        <a href="https://github.com/apple/swift-metrics/tree/1.2.1/Sources/CoreMetrics/Metrics.swift#L436">Show on GitHub</a>
                      </div>
                    </section>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </section>

      </article>
    </div>
    <section class="footer">
      <p>&copy; 2020 <a class="link" href="https://github.com/apple/swift-metrics" target="_blank" rel="external">SwiftMetrics team</a>. All rights reserved. (Last updated: 2020-02-28)</p>
      <p>Generated by <a class="link" href="https://github.com/realm/jazzy" target="_blank" rel="external">jazzy ♪♫ v0.13.1</a>, a <a class="link" href="https://realm.io" target="_blank" rel="external">Realm</a> project.</p>
    </section>
  </body>
</div>
</html>
